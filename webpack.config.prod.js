import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const GLOBALS = {
  'process.env.NODE_ENV': JSON.stringify('production')
};

// debug: shows debug info, devtool: ?, noinfo: displays a list of all the files it bundles
export default {
  debug: true,
  devtool: 'source-map',
  noInfo: false,

  // all of the middleware
  entry: './src/index', // This is the apps actual entry point. Should be last since order is critcal.

  // handles how to bundle based on platform
  target: 'web',

  // In the dev environment, it does not actually generate files, but emulates the file structure in memory. '__dirname' will get root folder
  output: {
    path: __dirname + '/dist', // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: 'bundle.js'
  },

  // Where is the code to bundle?
  devServer: {
    contentBase: './dist'
  },

  // What plugins to use
  plugins: [
   new webpack.optimize.OccurrenceOrderPlugin(), // Optimizes the order that our files are bundled in for optimal minification
   new webpack.DefinePlugin(GLOBALS), // Define variables that are made available to the libraries webpack is bundling.
   new ExtractTextPlugin('styles.css'), // Allows us to extract our CSS into a separate file.
   new webpack.optimize.DedupePlugin(), // eliminates duplicate packages in our bundle to keep bundle size a small as possible.
   new webpack.optimize.UglifyJsPlugin() // minify JS
  ],

  // the types of files you want Webpack to handle.
  module: {
    loaders: [
      {test: /\.js$/, include: path.join(__dirname, 'src'), loaders: ['babel']}, // javascript uses babel to transpile
      {test: /(\.css)$/, loaders: ExtractTextPlugin.extract("css?sourceMap")},

      // last 4 lines are used as recommended settings for bootstrap to with font files
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
      {test: /\.(woff|woff2)$/, loader: 'url?prefix=font/&limit=5000'},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'}
    ]
  }
};
