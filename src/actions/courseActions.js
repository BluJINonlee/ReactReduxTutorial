import * as types from './actionTypes';
import courseApi from '../api/mockCousreApi';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';

// because of the async nature, the actions are suffixed with success because they are only called if successful, there is usually a failure one too.
export function loadCoursesSuccess(courses) {
  return { type: types.LOAD_COURSES_SUCCESS, courses }; // this syntax is weird but it means if the the variable name is the same as the variable used as the variable. Same as { type: 'CREATE_COURSE', course: course }
}

export function createCourseSuccess(course) {
  return { type: types.CREATE_COURSE_SUCCESS, course }; // this syntax is weird but it means if the the variable name is the same as the variable used as the variable. Same as { type: 'CREATE_COURSE', course: course }
}

export function updateCourseSuccess(course) {
  return { type: types.UPDATE_COURSE_SUCCESS, course }; // this syntax is weird but it means if the the variable name is the same as the variable used as the variable. Same as { type: 'CREATE_COURSE', course: course }
}

// thunks. A thunk always returns a function that accepts a dispatch. Their functions will always return a return function(dispatach)
export function loadCourses() {
  return function(dispatch) {
    // returns a promise
    dispatch(beginAjaxCall());
    return courseApi.getAllCourses() // you can also make direct ajax calls here, but abstracting this allows you to easily change the import statement at the top of the page.
    .then( courses => { dispatch(loadCoursesSuccess(courses)); })

    .catch( error => { throw(error); });
  };
}


export function saveCourse(course) {
  return function(dispatch, getState){ // there is an option 'getState' parameter that allows you get the state: return function(dispatch, getState)
    
    dispatch(beginAjaxCall());
    return courseApi.saveCourse(course)
      .then(savedCourse => { savedCourse.id ? dispatch(updateCourseSuccess(savedCourse)) : dispatch(createCourseSuccess(savedCourse)); })
      .catch(error => {
        dispatch(ajaxCallError(error)); 
        throw(error);
      });
    
    };
}