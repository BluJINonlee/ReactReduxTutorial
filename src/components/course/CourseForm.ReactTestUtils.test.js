import expect from 'expect';
import React from 'react';
import TestUtils from 'react-addons-test-utils';
import CourseForm from './CourseForm';

function setup(saving) {
    let props = {
        course: {}, saving: saving, errors: {},
        onSave: () => {},
        onChange: () => {}
    };

    let renderer = TestUtils.createRenderer();
    renderer.render(<CourseForm {...props}/>);
    let output = renderer.getRenderOutput();

    return {
        props,
        output,
        renderer
    };
}

// The first argument give the name of the group of tests, while the second argument is a function that has the tests
describe('CourseForm via React Test Utils', () => {
    // the first argument is the name of the individula tests, and the second argument is the function that has the test
    it('renders form and h1', () => {
        const  { output } = setup();
        expect(output.type).toBe('form');
        let [ h1 ] = output.props.children;
        expect(h1.type).toBe('h1');
    });

    it('save button is laleled "Save" when not saving', () => {
        const { output } = setup(false);
        const submitButton = output.props. children[5];
        expect(submitButton.props.value).toBe('Save');
    });

    it('save button is laleled "Saving..." when saving', () => {
        const { output } = setup(true);
        const submitButton = output.props. children[5];
        expect(submitButton.props.value).toBe('Saving...');
    });


}); 