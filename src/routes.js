import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import HomePage from './components/home/HomePage';
import AboutPage from './components/about/AboutPage';
import CoursesPage from './components/course/CoursesPage';
import ManageCoursePage from './components/course/ManageCoursePage'; //eslint-disable-line import/no-named-as-default

export default (
	<Route path="/" component={App}> // Always load the App component, and nest the others under. Pass them as children based on routing
		<IndexRoute component={HomePage} /> // When these is just a route path that should be followed. That is, if someone just goes to /, go here (in this case, HomePage)
		<Route path = "about" component = {AboutPage} /> // if route is /about
		<Route path = "courses" component = {CoursesPage} />
		<Route path = "course(/:id)" component = {ManageCoursePage} />
	</Route>
);
