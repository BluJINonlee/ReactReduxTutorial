/* eslint-disable import/default */
import 'babel-polyfill'; // there's a set of features in ES6 that Babel cannot transpile, so for those, you need to use a poly fill
import React from 'react';
import { render } from 'react-dom';
import configureStore from './store/configureStore';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import { loadCourses } from './actions/courseActions';
import { loadAuthors } from './actions/authorActions';
import './styles/styles.css'; // Webpack can import CSS files too and will bundle it intelligently!
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/toastr/build/toastr.min.css';

const store = configureStore();
store.dispatch(loadCourses()); // calls the loadCourse() action on load
store.dispatch(loadAuthors());

render(
	<Provider store = {store}>
		<Router history={browserHistory} routes={routes} />
	</Provider>,
	document.getElementById('app')
);
