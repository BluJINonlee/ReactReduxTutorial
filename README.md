# React + Redux Tutorial
This tutorial is used to set up an environment that uses modern web technology

## Prerequisites
* You know the react basics
  * Add more to this

## Environment

### Production Dependencies
* babel-polyfill: Polyfill for Babel features that cannot be transpiled
* bootstrap: CSS Framework
* jquery:	Only used to support toastr
* react:	React library
* react-dom: React library for DOM rendering
* react-redux: Redux library for connecting React components to Redux
* react-router: React library for routing
* react-router-redux: Keep React Router in sync with Redux application state
* redux: Library for unidirectional data flows
* redux-thunk:	Async redux library
* toastr:	Display messages to the user

### Development Dependencies

* babel-cli: Babel Command line interface
* babel-core: Babel Core for transpiling the new JavaScript to old
* babel-loader: Adds Babel support to Webpack
* babel-plugin-react-display-name:	Add displayName to React.createClass calls
* babel-preset-es2015: Babel preset for ES2015
* babel-preset-react: Add JSX support to Babel
* babel-preset-react-hmre: Hot reloading preset for Babel
* babel-register: Register Babel to transpile our Mocha tests
* cheerio: Supports querying DOM with jQuery like syntax - Useful in testing and build process for HTML * manipulation
* colors: Adds color support to terminal
* compression: Add gzip support to Express
* cross-env: Cross-environment friendly way to handle environment variables
* css-loader: Add CSS support to Webpack
* enzyme: Simplified JavaScript Testing utilities for React
* eslint: Lints JavaScript
* eslint-plugin-import: Advanced linting of ES6 imports
* eslint-plugin-react: Adds additional React-related rules to ESLint
* eslint-watch: Add watch functionality to ESLint
* eventsource-polyfill: Polyfill to support hot reloading in IE
* expect: Assertion library for use with Mocha
* express: Serves development and production builds
* extract-text-webpack-plugin: Extracts CSS into separate file for production build
* file-loader:	Adds file loading support to Webpack
* jsdom: In-memory DOM for testing
* mocha: JavaScript testing library
* nock: Mock HTTP requests for testing
* npm-run-all:	Display results of multiple commands on single command line
* open: Open app in default browser
* react-addons-test-utils: Adds React TestUtils
* redux-immutable-state-invariant:	Warn when Redux state is mutated
* redux-mock-store: Mock Redux store for testing
* rimraf: Delete files
* style-loader: Add Style support to Webpack
* url-loader: Add url loading support to Webpack
* webpack: Bundler with plugin system and integrated development server
* webpack-dev-middleware: Adds middleware support to webpack
* webpack-hot-middleware: Adds hot reloading to webpack

## IDE
I personally user [Atom](http://atom.io) with the following packages
* Add packages here

## NPM Scripts
NPM scripts are used to automate much of the manual commands you would, otherwise, have to run manually.
 They are:

* Easy to learn
* Simplified
* No extra layer of abstraction so you can know exactly what's going on
* No dependencies on plugin author
* Simpler debugging
* Better documentation
* Allow you to make command line calls
* Utilize NPM packages
* Call separate scripts that use Node.js

To learn the benefits of NPM over gulp, see [here](bit.ly/npmvsgulp).

### How to Use
In your package.json file, create an object attribute called `scripts`. This is were you can create different scripts given a scenario, the most common being `start`:
```JavaScript
"scripts": {
  "start": "babel-node tools/srcServer.js"
}
```
The `start` script points to a javascript file. This file will run when you type the `npm start` command in the root folder of your project.

You can also assign command to perform before and after the `npm start` by adding a  `prestart` script and a `poststart` script. All of them together can look something like this:
```JavaScript
"scripts": {
  "prestart": "babel-node tools/startMessage.js",
  "start": "babel-node tools/srcServer.js"
}
```
**TODO: Add npm-run-all and parallel script documentation**

## Webpack

Create a `wepack.config.js` file in your root project folder. Optionally, you can create a `webpack.config.dev.js` to separate the production bundling and the dev bundling of your application.

Inside, there is a simple object that is exported with expected properties that will determine how Webpack bundles your application.



## Babel
Babel uses presets to transpile from one set of code to another. It uses a `.bablerc` file to configure it's presents. Here is an example:

```json
{
  "presets": ["react", "es2015"],
  "env": {
    "development": {
      "presets": ["react-hmre"]
    }
  }
}
```

The `preset` attribute is an array of Babel presets. You can also define what environment by defining tiers in the `env` attribute

Remember to `npm install` the present to use them To learn more about these preset, visit their site [here](http://babeljs.io)
To run it to transpile a file use the `babel-node` command, then the file displayName

### Example
```javascript
babel-node tool/srcServer.js // this tels babel transpile srcServer.js from ES6 to ES5
```

## Express
Exress is used to configure our webserver that serves up the files in our source directory


## ESLint
ESLint is a linting tool that assists writing JavaScript code that is errorlist and within best practices. It uses a json file placed in the root of the project folder called `.eslintrc` to configure its rules. You can use additional plugins to extend its capabilities.

```json
"extends": [
  "eslint:recommended",
  "plugin:import/errors",
  "plugin:import/warnings"
],
"plugins": [
  "react"
]
```


You can also override default rules in the `rules` attribute of the file. For example, in
```json
"rules": {
  "quotes": 0,
  "no-console": 1,
  "no-debugger": 1,
  "no-var": 1,
  "semi": [1, "always"],
  "no-trailing-spaces": 0,
  "eol-last": 0,
  "no-unused-vars": 0,
  "no-underscore-dangle": 0,
  "no-alert": 0,
  "no-lone-blocks": 0,
}
```
a 0 value means off, 1 means warning, and 2 means error meaning it will not build when in violation of this rule.

ESLint has support for ES6 out of the box, so you can have it parse for ES6 in the `parserOptions` attribute:

```json
"parserOptions": {
  "ecmaVersion": 6,
  "sourceType": "module",
  "ecmaFeatures": {
    "jsx": true
 }
}
```

ESLint will look out certain environmental specifics if they are listed in the `env` attribute:
```json
"env": {
  "es6": true,
  "browser": true,
  "node": true,
  "jquery": true,
  "mocha": true
}
```

To use it in your start build, in your `package.json` file, add a new script called `lint`:
```json
"scripts": {
  "lint": "node_modules/.bin/esw webpack.config.* src tools",
}
```
Notice in this example, there is a `node_modules/.bin/esw` in the command. The reason is because eslint, by default, does not have a watcher (ie it would not catch changes made/refreshes), so we used a module call ESLint Watcher. This command adds a lint watcher to the webpack.config files (both dev and prod), src, and tools. To run this, run the command `npm run lint`. However, it does not run automatically, so we'll have to pass it with an argument, and we'll do it in another script.

```json
"scripts": {
  "lint": "node_modules/.bin/esw webpack.config.* src tools",
  "lint:watch": "npm run lint -- --watch",
  "prestart": "babel-node tools/startMessage.js",
  "start": "babel-node tools/srcServer.js"
}
```
What the `lint:watch` script does is call the lint script above it. The `-- --watch` allows us to pass the watch flag within a script.


# Known errors
If you ever get this messages

```
Error: listen EADDRINUSE :::3000
    at Object.exports._errnoException (util.js:1018:11)
    at exports._exceptionWithHostPort (util.js:1041:20)
    at Server._listen2 (net.js:1258:14)
    at listen (net.js:1294:10)
    at Server.listen (net.js:1390:5)
    at EventEmitter.listen (D:\developments\Tutorials\ReactReduxTutorial\node_modules\express\lib\application.js:617:24)

    at Object.<anonymous> (srcServer.js:25:5)
    at Module._compile (module.js:570:32)
    at loader (D:\developments\Tutorials\ReactReduxTutorial\node_modules\babel-register\lib\node.js:158:5)
    at Object.require.extensions.(anonymous function) [as .js] (D:\developments\Tutorials\ReactReduxTutorial\node_modules\babel-register\lib\node.js:168:7)
ERROR: open:src: None-Zero Exit(1);
```

it is likely because you did not close the server properly and it's still running. To close, kill node.js in the task manager.


## React
There are four ways you can create a react component:

### ES5 createClass
This is the original way to create a React Class.
```javascript
var HelloWorld = React.createClass({
  render: function () {
    return (
      <h1>Hello World</h1>
    );
  }
});
```

### ES6 class

#### 1. No autobind
This works fine in ES5 createClass, but not in ES6: `<div onClick={this.handleClick}></div>`

ES5 autobinded for you, where in ES6, you must explicitly bind as such: `<div onClick={this.handleClick.bind(this)}></div>`

Or the more recommended binding in the constructor:

```javascript
class Contacts extends React.Component {
  constructor(props){
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
}
```
#### 2. Proptypes and Default props are declared separately below your class definition
Assuming a `HelloWorld` class with a `name` string property that is required, this is how the defining of props might look:
```javascript
HellowWorld.propTypes = {
  name: React.PropTypes.string.isRequired
};
```

#### 3. Set initial state is set in Contructor

### ES5 Stateless function
Stateless fucntions are used to create simple component that do not manage state, use component lifecycle, or do performance optimization. They have no state and they get their data solely from props which are immutable.

```javascript
var HelloWorld = function(props) {
  return (
    <h1>Hello World</h1>
  );
};
```

### ES6 Stateless Function

The only thing that is different in ES6 is instead of declaring a variable, we declare a constance since they should technically never change. IN ES6, actually, the `var` keyword should be avoided. Instead, use `const` or `let`.

```javascript
var HelloWorld = (props) => {
  return (
    <h1>Hello World</h1>
  );
};
```

There are 9 major benefits:

1. No class needed
2. Avoid `this` and `bind` keyword
3. Enforces best practices (focused on UI rather than behavior)
4. High signal-to-noise ratio
5. Enhanced code completion / intellisense
6. Bloated components are obvious
7. Easy to understand
8. Easy to tests
9. Performance

## When to use class components and when to use stateless components

* Class
  * State
  * Refs to underlying DOM (Ref will always return `null` in a stateless component)
  * Lifecycle methods
  * Child funcion (for performance)
* Stateless components
  * Everywhere else :)


## Other less popular ways
* Object.create
* Mixins
* Parasitic components
* StampIt

More info: bit.ly/react-define-component

## Container vs Presentation Components
* Container (Backend)
  * Little to no markup
    * Should be concerned with behavior
  * Passing data and actions to their children
  * Know about Redux
  * Often stateful
* Presenation (Frontend)
	* Nearly all markup
    * They shouldn't have logic inside
  * Receive data and actions via props
  * Doesn't know about Redux
    * Makes presentation components reusable and easier to understand and just uses props
  * Typically stateless function components

**Most of your components should be _Presentation_**


### Synonyms
**Container**
* Smart
* Stateful
* Controller View

**Presenation**
* Dumb
* Stateless
* View

> When you notice that some components don't use props they receive but merely forward them down...it's a good time to introduce some container components.
~ Dan Abramov

Despite the majority of components should be Presentational, Containers can be used throughout your app to separate certain functions of your app.

### Best Practices

There are two popular ways to organize components:

* Containers and Presentational components
* By page/function


## React Router


__Note:__
index.html -> Index.js => routes.js => ./components/App.js


## Redux

Redux is useful for applications that have complex dataflows, inter-component communication, non-heirarchical data, or many action, or have the same data used in multiple places

> You'll know when you need flux or Redux. If you aren't sure you need it, you don't need it.

~ Pete Hunt on Flux and Redux


There are 3 core princple for Redux:

1. **One immuntable store**: The state cannot be changed.
2. **Actions trigger changes**: The only way to mutate state is to emit an action which describes the user's intent.
3. **Reducers update state**: State is changed by pure functions.

### Redux Parts

#### Action
_Actions_ are the events happening in the application. It describes user intent in an object. It can have any properties, but it must at least have a `type` property. It can be anything that will serialize into JSON, so almost all Javascript types except for functions and promises.

Example:
```javascript
{
 type: RATE_COURSE,
 rating: 5 // the data
}
```
Actions are created by helper functions called _Action Creators_. Usually the name of the action creator is the same as the `type` property of the event. Though they are not required, this convention is recommended because the spot where you dispatch does not need to know the action creator structure.
Example:
```javascript
rateCourse(rating) {
     return {
       type: RATE_COURSE,
       rating: 5
     }
}

```


### Reducer
The action emitted will be handled by a reducer. This is just a fancy name for a function that returns new state. It will receive the current state and an action, and return the new state. Handle the state changes. That's it.

```javascript
  function appReducer(state = defaultState, action) {
    switch(action.type) {
      case RATE_COURSE:
        //return new state
    }
  }
```
In this example, this is how incrementing a counter might look like.
```JavaScript
  function myReducer(state, action) {
    switch (action.type) {
      case 'INCREMENT_COUNTER':
        return (
          Object.assign(
            {}, // create a new empty object
            state, // include in it a copy of state's properties
            { counter: state.counter + 1 }) // but make the counter property increase by 1
        );
    }
  }
```

Whenever an action is dispatched, all of the reducers are called, but only reducers that handle that specific action will return a new state, all other reducers will simply return the same state.

**Note**: Reducers must be pure functions. _Pure_ means that they should never produce unexpected results and given the same arguments, you should ***ALWAYS*** get the same results from it.

#### The sins of a reducer function:
* Mutate arguemnts
* Perform side effects (API calls or routing transitions)
* Call non-pure functions

### Store
When a new state is returned from a reducer, the store is updated. The store is a central repository for a certain object type. It can be thought of a client side database. When it update, React re-renders any components using its data. They are connected by `react-redux`. There should only be one store.

To create a store, you call `createStore()` in your application entry point and pass in the reducer.


#### `store.dispatch(action)`
#### `store.subscribe(listener)`
#### `store.getState()`
#### `store.replaceReducer(nextReducer)`

The store is immutable. This means its value cannot be changed, only replaced. This means to update a store you must give it an entirely new object, instead of telling it just to updated this new property. To have to manually assign every property again would be very impractical. To make this easier, use the ES6 (EcmaScript 2015) function `Object.assign()`. It accepts the Object you want to assign the new object to, the object you had usually wanted to change if it was mutable, and an object of the properties you want to change from the aforementioned object you want to copy.

example
```JavaScript
Object.assign({}, state, {role: 'admin'}); // this would copy state into a blank object, except change role in that object to the new value of 'admin'
```
**Warning:** Done forget the first property of the empty object. If not, this will simply mutate the object in the second property. Also, Babel cannot transpile this object, so make sure to include polyfill in the root of your application.

#### Why Immunitabiliy?
3 reasons:
* Clarity: You know exactly what is changing your state.
* Performance: To tell if a state change, it can simply check the object refernce rather than comparing each attribute.
* [Great Debugging Tools](https://github.com/gaearon/redux-devtools)

### How to Connect React & Reducer

#### React-Reduc connect

`connect(mapStateToProps, mapDispatchToProps)`

mapStateToProps: What state should I expose as props? When defined, the component It will subscribe to redux store updates. Anytime it updates, this function will be called. This function returns an object. Each property defined will become a property on your container component. In short, it defines what state is available to your component via props. You can tranform your state here so it's convenient for the app to use.

##### Example
```JavaScript
  function mapStateToProps(state) {
    return {
      appState: state
    };
  }
```
This simple function makes all of your state available to this component. for a more complex app, you may not want to do this and just take the part of state that you need for that part of the application. In the component you would access that state returned by this function by using `this.props.appstate`


mapDispatchToProps: What actions do I want to expose as props.


##### Example
```JavaScript
function mapDispatchToProps(dispatch){
  return {
    action: bindActionCreators(actions, dispatch)
  };
}
```
### 3 Ways to Handle mapDispatchToProps
1. **Ignore it. Use Dispatch:** `mapDispatchToProps` is an optional parameter. If you decided not to make it, you can call dispatch manually like this: `this.props.dispatch(loadcourse());`.
2. **Manually wrap:**

```JavaScript
function mapDispatchToProps(dispatch) {
  return {
    loadCourses: () => {
      dispatch(loadCourses());
    },
    createCourse: (course) => {
      dispatch(createCourse(course));
    },
    updatedCourse: (course) => {
      dispatch(updateCourse(course));
    }
  }
}
// called in components as this.props.loadCourses
```
3. **Use `bindActionCreators(actions, dispatch)`:** This function ships with Redux to handle this redundancy for you. The `bindActionCreators` function will wrap all the actions passed to it in a dispatch call for you:

```JavaScript
function mapDispatchToProps(dispatch){
  return {
    action: bindActionCreators(actions, dispatch)
  };
}
// this.props.actions.loadCourses
```
However, the prop created by these created differently. The prop in this action is called `actions`, where option number 2 exposes props called `loadCourses`,`createCourse`, and `updateCourse`. The difference is only slightly different. Where in example 2, you will call a function like `this.props.loadCourses`, there you will call it as `this.props.actions.loadCourses`. Overall, they will present that same result.

The advantage of option 2 & 3 have over 1 is the child components do not have to be aware of Redux at all, where one, you have to call the Redux method everytime you want to perform an action.

### Redux Flow
1. React
2. Action
3. Store
4. Reducer
5. React

#### A Chat with Redux

**React:** Hey, _CourseAction_, someone clicked this "Save Course" button.

**Action:** Thanks, _React_! I will dispatch an action so reducers that care can update state.

**Reducer:** Ah, thanks, _Action_. I see you passed me the current state and the action to perform. I'll make a new copy of the state and return it.

**Store:** Thanks for updating the state, _Reducer_. I'll make sure that all connected components are aware.

**React-Redux:** Woah, thanks for the new date, _Mr.
Store_. I'll now intellegently determine if I should tell _React_ about this change so that i only has to bother with updating the UI when necessary.

**React:** Ooo! Shiny new data has been passed down via props from the _Store_! I'll update the UI to reflect this!


## Thunk
Normally, we can only return an object for our action creators, but with Thunk, we can return a fucntion instead.`Thunk` is actually a computer science term. *Thunk* is a function that wraps an expression in order to delay it's evalution. 


```javascript
export function deleteAuthor(authorId) {
  return dispatch => {
    return AuthorApi.deleteAuthor(authorId)
    .then( () = > {dispatch(deleteAuthor(authorId))} )
    .catch(handleError);
  };
}
```
>*In this example, the `deleteAuthor()` function is wrapping our dispatch function so that dispatch can run later.*


## Production Build
Our development process does not generate any actual files. Everything is being served by webpack. It just reads the files in the source directory and serves the process files from memory. For production, we need to run real "physical" files to the file system so a web server can serve them up. This is often organized by having a `/src` folder (for source code) and a `/dist` folder (for production files for distribution).

### Dist
When broken down, the `/dist` folder should have only 3 minified and bundled files:
* `index.html`: Entry point of the website that references `bundle.js` and `styles.css`
* `bundle.js`: Contains all the JavaScript
* `styles.css`: Contains all the global stylings

### Productino Build Process
The production build process should do the following:

* Lint and run tests
* Bundle and minify JS and CSS
* Generate JS and CSS sourcemaps
* Excludes dev-specific concerns (e.g. hot-reloading)
* Build React in production mode
* Open prod build in browser