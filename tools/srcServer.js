import express from 'express';
import webpack from 'webpack';
import path from 'path';
import config from '../webpack.config.dev';
import open from 'open';

/* eslint-disable no-console */

const port = 3000;
const app = express(); //
const compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true, // will not show information on the command line as it runs
  publicPath: config.output.publicPath // the path the at requestor will see
}));

app.use(require('webpack-hot-middleware')(compiler)); // for hot reloading

// the * means that all pages will be returned to the the index.html file since it's a SPA
app.get('*', function(req, res) {
  res.sendFile(path.join( __dirname, '../src/index.html'));
});

app.listen(port, function(err) {
  if (err) {
    console.log(err);
  } else {
    open(`http://localhost:${port}`); // opens the browser
  }
});
