import webpack from 'webpack';
import path from 'path';


// debug: shows debug info, devtool: ?, noinfo: displays a list of all the files it bundles
export default {
  debug: true,
  devtool: 'inline-source-map',
  noInfo: false,

  // all of the middleware
  entry: [
    'eventsource-polyfill', // necessary for hot reloading with IE
    'webpack-hot-middleware/client?reload=true', // hot reloading
    path.resolve(__dirname, 'src/index') // This is the apps actual entry point. Should be last since order is critcal.
  ],

  // handles how to bundle based on platform
  target: 'web',

  // In the dev environment, it does not actually generate files, but emulates the file structure in memory. '__dirname' will get root folder
  output: {
    path: __dirname + '/dist', // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: 'bundle.js'
  },

  // Where is the code to bundle?
  devServer: {
    contentBase: path.resolve(__dirname, 'src')
  },

  // What plugins to use
  plugins: [
    new webpack.HotModuleReplacementPlugin(), // Allows you to replace modules without having to do a full browser refresh
    new webpack.NoErrorsPlugin() // Will keep errors from breaking the hot loading experience.
  ],

  // the types of files you want Webpack to handle.
  module: {
    loaders: [
      {test: /\.js$/, include: path.join(__dirname, 'src'), loaders: ['babel']}, // javascript uses babel to transpile
      {test: /(\.css)$/, loaders: ['style', 'css']},

      // last 4 lines are used as recommended settings for bootstrap to with font files
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
      {test: /\.(woff|woff2)$/, loader: 'url?prefix=font/&limit=5000'},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'}
    ]
  }
};
